# PROBLEM 2

max_num = 4000000
fib_list = [1,1]
total_sum = 0

while fib_list[-1] < max_num:
	fib_list.append(sum(fib_list[-2:]))

for fib in fib_list:
	if fib%2 == 0:
		total_sum += fib

print(f'Problem 2: Solution = {total_sum}')