# PROBLEM 4

from itertools import product

max_num = 0

def is_palindrome(num):
	"test whether integer is a palindrome or not"
	num_list = list(str(num))
	return num_list == list(reversed(num_list))

for i, j in product(range(100, 1000), range(100, 1000)):
	num = i * j
	if is_palindrome(num) and num > max_num:
		max_num = num

print(f'Problem 4: Solution = {max_num}')