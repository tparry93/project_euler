# PROBLEM 9

def is_pythagorean_triplet(a, b, c):
	"test for a Pythagorean Triplet"
	if a > b or b > c:
		return False

	return a * a + b * b == c * c

prod = None

for a in range(1, 1000):
	for b in range(a, 1000):
		for c in range(b, 1000):
			if a + b + c == 1000:
				if is_pythagorean_triplet(a, b, c):
					prod = a * b * c
					break
		if prod is not None: break
	if prod is not None: break

print(f'Problem 9: Solution = {prod}')