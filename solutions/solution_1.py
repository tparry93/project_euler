# PROBLEM 1

max_num = 999
total_sum = 0

for i in range(1, max_num+1):
	if i%3 == 0 or i%5 == 0:
		total_sum += i

print(f'Problem 1: Solution = {total_sum}')