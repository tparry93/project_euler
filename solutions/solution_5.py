# PROBLEM 5

max_num = 20
num = 20

while True:
	success = True
	for i in range(1, max_num+1):
		if num%i != 0:
			success = False
			break

	if success:
		break
	else:
		num += max_num

print(f'Problem 5: Solution = {num}')