# PROBLEM 6

max_num = 100

sum_of_squares = sum(i*i for i in range(1, max_num+1))
square_of_sum = sum(range(1, max_num+1)) * sum(range(1, max_num+1))

print(f'Problem 6: Solution = {square_of_sum - sum_of_squares}')